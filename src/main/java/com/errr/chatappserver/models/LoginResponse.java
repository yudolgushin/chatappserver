package com.errr.chatappserver.models;

public class LoginResponse {
    public String accessToken;
    public String userId;

    public LoginResponse(String accessToken, ChatUser user) {

        this.accessToken = accessToken;
        this.userId = user.id;
    }
}
