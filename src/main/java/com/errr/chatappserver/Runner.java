package com.errr.chatappserver;

import com.errr.chatappserver.verticles.main.ServerVerticle;
import io.vertx.core.Vertx;

import java.sql.SQLException;

public class Runner {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(ServerVerticle.class.getName());
    }
}
