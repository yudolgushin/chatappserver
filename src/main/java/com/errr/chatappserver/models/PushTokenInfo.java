package com.errr.chatappserver.models;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class PushTokenInfo {
    public static final Type LIST_TYPE = new TypeToken<List<PushTokenInfo>>() {
    }.getType();

    public String userId;
    public String pushToken;

    public PushTokenInfo(String userId, String pushToken) {
        this.userId = userId;
        this.pushToken = pushToken;
    }
}
