package com.errr.chatappserver;

import com.errr.chatappserver.models.AccessTokenInfo;
import com.errr.chatappserver.models.ChatUser;
import com.errr.chatappserver.models.PushTokenInfo;
import com.google.gson.Gson;
import org.sqlite.SQLiteConfig;

import java.sql.*;
import java.util.List;

public class Db {
    private static final String USERS = "users";
    private static final String ACCESS_TOKENS = "access_tokens";
    private static final String PUSH_TOKENS = "push_tokens";
    Gson gson;
    Connection conn;
    Statement state;

    public Db() throws ClassNotFoundException, SQLException {
        gson = JsonUtils.getGson(false);
        Class.forName("org.sqlite.JDBC");
        SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true);
        String name = "Db.db";
        conn = DriverManager.getConnection("jdbc:sqlite:" + name);
        state = conn.createStatement();
        createTable(USERS);
        createTable(ACCESS_TOKENS);
        createTable(PUSH_TOKENS);

    }


    public void createTable(String tableName) throws SQLException {
        state.execute("CREATE TABLE IF NOT EXISTS " + tableName + " (val TEXT)");
    }

    public List<ChatUser> getAllUsers() throws SQLException {
        PreparedStatement pr = conn.prepareStatement("SELECT json_group_array(json(val)) FROM " + USERS);
        List<ChatUser> list = gson.fromJson(pr.executeQuery().getString(1), ChatUser.LIST_TYPE);
        return list;
    }

    public ChatUser findUserByLogin(String login) throws SQLException {
        PreparedStatement prep = conn.prepareStatement("SELECT json_group_array(json(val)) FROM " + USERS +
                " WHERE UPPER(json_extract(val, '$.login')) = ? ");
        prep.setString(1, login.toUpperCase());
        List<ChatUser> o = gson.fromJson(prep.executeQuery().getString(1), ChatUser.LIST_TYPE);
        if (!o.isEmpty()) {
            return o.get(0);
        }
        return null;

    }

    public ChatUser findUserByLoginAndPassword(String login, String password) throws SQLException {
        PreparedStatement prep = conn.prepareStatement("SELECT json_group_array(json(val)) FROM " + USERS +
                " WHERE UPPER(json_extract(val, '$.login')) = ? AND json_extract(val, '$.password') = ? ");
        prep.setString(1, login.toUpperCase());
        prep.setString(2, password);
        List<ChatUser> o = gson.fromJson(prep.executeQuery().getString(1), ChatUser.LIST_TYPE);
        if (!o.isEmpty()) {
            return o.get(0);
        }
        return null;
    }


    public void addNewAccessToken(String id, String token) throws SQLException {
        AccessTokenInfo accessTokenInfo = new AccessTokenInfo(id, token);
        String val = gson.toJson(accessTokenInfo);
        PreparedStatement prep = conn.prepareStatement("INSERT INTO " + ACCESS_TOKENS + " (val) VALUES (?)");
        prep.setString(1, val);
        prep.execute();
    }

    public void insertChatUser(ChatUser chatUser) throws SQLException {//TODO prepared statement        !
        String val = gson.toJson(chatUser);
        PreparedStatement prep = conn.prepareStatement("INSERT INTO " + USERS + " (val) VALUES (?)");
        prep.setString(1, val);
        prep.execute();
    }

    public ChatUser findUserByAccessToken(String accessToken) throws SQLException { //TODO use one query   !
        PreparedStatement prep = conn.prepareStatement("SELECT u.val FROM " + USERS + " u, access_tokens at " +
                "WHERE json_extract(u.val, '$.id') = json_extract(at.val, '$.userId') and json_extract(at.val, '$.accessToken') = ?");
        prep.setString(1, accessToken);
        ResultSet resultSet = prep.executeQuery();

        ChatUser user = null;

        if (!resultSet.isClosed()) {
            user = gson.fromJson(resultSet.getString(1), ChatUser.class);
            resultSet.close();
        }
        return user;

    }

    private ChatUser findUserById(String id) throws SQLException {    //TODO  maximum shit    !
        PreparedStatement prep = conn.prepareStatement("Select * from " + USERS + " where json_extract(val, '$.id') = ?;");
        prep.setString(1, id);
        ChatUser user = gson.fromJson(prep.executeQuery().getString(1), ChatUser.class);
        return user;
    }


    public void savePushToken(String userId, String pushToken) throws SQLException {
        //TODO delete all other rows which contains this pushtoken
        PreparedStatement prep = conn.prepareStatement("DELETE FROM " + PUSH_TOKENS + " where json_extract(val, '$.pushToken')=?");
        prep.setString(1, pushToken);
        prep.execute();

        PushTokenInfo pushTokenInfo = new PushTokenInfo(userId, pushToken);
        String val = gson.toJson(pushTokenInfo);
        prep = conn.prepareStatement("INSERT INTO " + PUSH_TOKENS + " (val) VALUES (?)");
        prep.setString(1, val);
        prep.execute();


    }

    public List<PushTokenInfo> getAllPushTokenForId(String toId) throws SQLException {
        PreparedStatement prep = conn.prepareStatement("SELECT json_group_array(json(val)) FROM " + PUSH_TOKENS +
                " WHERE json_extract(val, '$.userId') = ? ");
        prep.setString(1, toId);
        List<PushTokenInfo> list = gson.fromJson(prep.executeQuery().getString(1), PushTokenInfo.LIST_TYPE);

        return list;
    }

    public void deletePushTokensById(String id) throws SQLException {

        PreparedStatement prep = conn.prepareStatement("DELETE FROM " + PUSH_TOKENS + " where json_extract(val, '$.userId')=?");
        prep.setString(1, id);
        prep.execute();
    }

    public ChatUser getUserById(String id) throws SQLException {
        PreparedStatement prep = conn.prepareStatement("Select * from " + USERS + " where json_extract(val, '$.id') = ?");
        prep.setString(1, id);
        ChatUser user = gson.fromJson(prep.executeQuery().getString(1), ChatUser.class);
        return user;
    }

    public void refreshDisplayNameUser(ChatUser user) throws SQLException {
        ChatUser userTmp = findUserById(user.id);
        userTmp.displayName = user.displayName;

        PreparedStatement prep = conn.prepareStatement("Update " + USERS + " set val = ? where json_extract(val, '$.id') = ?");
        prep.setString(1, new Gson().toJson(userTmp));
        prep.setString(2, user.id);
        prep.execute();


    }

    public void updateImg(ChatUser user) throws SQLException {
        ChatUser userTmp = findUserById(user.id);
        userTmp.imgId = user.imgId;

        PreparedStatement prep = conn.prepareStatement("Update " + USERS + " set val = ? where json_extract(val, '$.id') = ?");
        prep.setString(1, new Gson().toJson(userTmp));
        prep.setString(2, user.id);
        prep.execute();

    }
}





