package com.errr.chatappserver.verticles.main;

import com.errr.chatappserver.JsonUtils;
import com.errr.chatappserver.Log;
import com.errr.chatappserver.Utils;
import com.errr.chatappserver.models.*;
import io.vertx.core.Handler;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.core.http.HttpServerResponse;
import io.vertx.rxjava.ext.web.RoutingContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class SyncHelper {
    private static final String LOG_TAG = "SyncHelper";
    private final ServerVerticle v;
    private Map<String, List<MessageUser>> pendingMessagesByUser = new HashMap<>();
    private Map<String, ActiveSync> activeSyncByUser = new HashMap<>();

    public SyncHelper(ServerVerticle v) {
        this.v = v;
    }


    void onSyncRequest(RoutingContext ctx, Buffer bodyBuffer) {
        HttpServerResponse response = v.getStandardResponse(ctx);
        try {
            SyncRequest req = JsonUtils.fromJson(bodyBuffer.toString(), SyncRequest.class);
            ChatUser user = v.checkAccessToken(req);
            SyncData syncData = new SyncData();

            Handler<Long> sendSyncData = event -> v.responseEnd(ctx, response, new BaseResponse(syncData));

            List<MessageUser> messages = pendingMessagesByUser.get(user.id);
            if (!Utils.isEmpty(messages)) {
                pendingMessagesByUser.remove(user.id);
                syncData.newMessages.addAll(messages);
                sendSyncData.handle(0L);
            } else {
                long timerId = v.getVertx().setTimer(25_000, event -> {
                    Log.d(LOG_TAG, "remove sync for " + user.login + " id " + user.id);
                    activeSyncByUser.remove(user.id);
                    sendSyncData.handle(event);
                });

                ActiveSync currentActiveSync = activeSyncByUser.get(user.id);
                if (currentActiveSync != null) {
                    RuntimeException e = new RuntimeException("Concurrent Syncs for one user " + user.login + " id " + user.id);
                    v.log(e);
                    currentActiveSync.fail(e);
                }
                currentActiveSync = new ActiveSync(v.getVertx(), timerId, sendSyncData, syncData, e -> v.responseEnd(ctx, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e)));
                Log.d(LOG_TAG, "put new sync for " + user.login + " id " + user.id);
                activeSyncByUser.put(user.id, currentActiveSync);
            }
        } catch (Exception e) {
            v.log(e);
            v.responseEnd(ctx, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
        }
    }

    public void onNewMessage(MessageUser msg) {
        ActiveSync activeSync = activeSyncByUser.get(msg.toUserId);
        if (activeSync != null) {
            activeSync.onNewMessage(msg);
            Log.d(LOG_TAG, "remove sync for " + "-" + " id " + msg.toUserId);
            activeSyncByUser.remove(msg.toUserId);
        } else {
            List<MessageUser> msgs = pendingMessagesByUser.getOrDefault(msg.toUserId, new ArrayList<>());
            msgs.add(msg);
            pendingMessagesByUser.put(msg.toUserId, msgs);
        }
    }
}
