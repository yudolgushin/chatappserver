package com.errr.chatappserver.models;


import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;

public class ChatUser {
    public static final Type LIST_TYPE = new TypeToken<List<ChatUser>>() {
    }.getType();

    public String id;
    public String login;
    public String password;
    public String displayName;
    public String imgId;


    public ChatUser(String login, String password) {
        this.login = login;
        this.password = password;
        this.id = UUID.randomUUID().toString();
    }
}
