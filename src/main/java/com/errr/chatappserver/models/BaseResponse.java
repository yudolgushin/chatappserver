package com.errr.chatappserver.models;

public class BaseResponse {
    public static final String UNEXPECTED_ERROR = "UNEXPECTED_ERROR";
    public static final String OK = "OK";
    public static final String FAILED_TO_UPLOAD_FILE = "FAILED_TO_UPLOAD_FILE";
    public String result;
    public Object value;
    public String errorMessage;

    public BaseResponse(String result, String value) {
        this.result = result;
        this.value = value;
    }

    public BaseResponse(Object value) {
        result = OK;
        this.value = value;
    }

    public BaseResponse(String result, Exception e) {
        this.result = result;
        this.errorMessage = "" + e.getClass().getSimpleName() + ": " + e.getLocalizedMessage();
    }
}
