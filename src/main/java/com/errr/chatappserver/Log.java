package com.errr.chatappserver;

import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
    private static Vertx vertx;

    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public static void setVertx(Vertx vertx) {
        Log.vertx = vertx;
    }
/*
    private static void publishError(MatrixError matrixError) {
        publishEventBus(Addresses.ERROR_OCCURRED, JsonUtils.getGson(false).toJson(matrixError));
    }*/

    private static EventBus publishEventBus(String address, Object message) {
        return vertx.eventBus().<String>publish(address, message);
    }

    public static void d(String logTag, String s) {
        final String out = logTag + " " + s;
        System.out.println(getDateMark() + " DEBUG " + out);
        //publishEventBus(Addresses.DEBUG_OUTPUT, out);
    }

    public static void e(String logTag, String s) {
        System.err.println(getDateMark() + " ERROR " + logTag + " " + s);
      //  publishError(new MatrixError(s, logTag));
    }

   /* public static void e(String logTag, MatrixError me, String additionalInfo) {
        if (me == null) {
            e(logTag, additionalInfo);
        } else {
            System.err.println(format.format(new Date(System.currentTimeMillis())) + " " + logTag +
                    " ERROR  httpCode: " + me.httpCode + " uri: " + me.requestUri + additionalInfo + " " + me.getLocalizedMessage());

            if (me.cause != null && me.httpCode != 502) {
                me.cause.printStackTrace();
            }
            me.tag = logTag;
            publishError(me);
        }
    }*/

    public static void e(String logTag, Throwable s) {
        e(logTag, s, null);
    }

    public static void e(String logTag, Throwable s, String additionalInfo) {
        System.err.println(getDateMark() + " ERROR " + logTag + " " + additionalInfo + " " + s.getLocalizedMessage());
        s.printStackTrace();
     //   publishError(new MatrixError(s, null, logTag, additionalInfo));
    }

    public static void w(String logTag, String s) {
        System.err.println(getDateMark() + " WARN " + logTag + " " + s);
    }

    private static String getDateMark() {
        return format.format(new Date(System.currentTimeMillis()));
    }
}
