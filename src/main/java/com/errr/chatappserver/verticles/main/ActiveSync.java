package com.errr.chatappserver.verticles.main;

import com.errr.chatappserver.Log;
import com.errr.chatappserver.models.MessageUser;
import com.errr.chatappserver.models.SyncData;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import rx.functions.Action1;

public class ActiveSync {
    private static final String LOG_TAG = "ActiveSync";
    private final Vertx vertx;
    private final long timerId;
    private final Handler<Long> sendSyncData;
    private final SyncData syncData;
    private final Action1<Exception> onFail;
    public static final boolean DEBUG_LOG = true;

    public ActiveSync(Vertx vertx, long timerId, Handler<Long> sendSyncData, SyncData syncData, Action1<Exception> onFail) {
        this.vertx = vertx;
        this.timerId = timerId;
        this.sendSyncData = sendSyncData;
        this.syncData = syncData;
        this.onFail = onFail;
        if (DEBUG_LOG) {
            Log.d(LOG_TAG, "Active sync created: timer id: " + timerId);
        }
    }

    public void fail(RuntimeException e) {
        vertx.cancelTimer(timerId);
        onFail.call(e);
        if (DEBUG_LOG) {
            Log.e(LOG_TAG, "Active sync failed: timer id: " + timerId + " " + e);
        }
    }

    public void onNewMessage(MessageUser msg) {
        syncData.newMessages.add(msg);
        sendSyncData.handle(0L);

        if (DEBUG_LOG) {
            Log.d(LOG_TAG, "Active sync onNewMessage: timer id: " + timerId + " " + msg);
        }
    }
}
