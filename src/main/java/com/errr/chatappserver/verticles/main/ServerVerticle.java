package com.errr.chatappserver.verticles.main;

import com.errr.chatappserver.Db;
import com.errr.chatappserver.JsonUtils;
import com.errr.chatappserver.Log;
import com.errr.chatappserver.Utils;
import com.errr.chatappserver.models.*;
import io.vertx.core.Handler;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.http.HttpServerResponse;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.RoutingContext;
import io.vertx.rxjava.ext.web.handler.LoggerHandler;
import io.vertx.rxjava.ext.web.handler.StaticHandler;

import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class ServerVerticle extends AbstractVerticle {
    private static final String LOG_TAG = "ServerVerticle";
    public static final boolean DEBUG = true;
    private Db db;
    private PushSenderHelper pushSenderHelper;
    public static final String WEBROOT_IMG = "./webroot/img/";
    private SyncHelper syncHelper;

    @Override
    public void start() throws Exception {
        super.start();
        db = new Db();

        pushSenderHelper = new PushSenderHelper(this);
        syncHelper = new SyncHelper(this);

        startWeb();


    }

    private void startWeb() {
        log("Starting Web Server");
        Router router = Router.router(vertx);
        router.route().handler(LoggerHandler.create());
        // TODO unrefisterPushToken ,  logout
        //TODO У одлного пуш токена не может быть два юзера

        router.post("/signUp").handler(signUp());
        router.post("/login").handler(login());
        router.post("/uploadUserAvatar").handler(io.vertx.rxjava.ext.web.handler.BodyHandler.create().setUploadsDirectory(WEBROOT_IMG));
        router.post("/uploadUserAvatar").handler(uploadImg());
        router.post("/logOut").handler(logOut());
        router.post("/getChatUsers").handler(getChatUsers());
        router.post("/refreshDisplayNameUser").handler(refreshDisplayNameUser());
        router.post("/getChatUserById").handler(getChatUserById());
        router.post("/sendMessage").handler(sendMessage());
        router.post("/registerPushToken").handler(registerPushToken());
        router.post("/sync").handler(sync());


        int oneDaySec = 60 * 60 * 24;
        int tenMinsSec = 60 * 10;
        router.route("/img/*").handler(StaticHandler.create().setCachingEnabled(true).setCacheEntryTimeout(oneDaySec * 1000).setMaxAgeSeconds(oneDaySec)
                .setWebRoot("./webroot/img/"));

        //noinspection ConstantConditions
        if (DEBUG) {
            router.route("/*").handler(StaticHandler.create().setCachingEnabled(false).setCacheEntryTimeout(1_000).setMaxAgeSeconds(2));
        } else {
            router.route("/*").handler(StaticHandler.create().setCachingEnabled(true).setCacheEntryTimeout(tenMinsSec * 1000).setMaxAgeSeconds(tenMinsSec));
        }

        int port = 9090;
        vertx.createHttpServer().requestHandler(router::accept).listen(port);
        log("Started Web Server at " + port);
    }

    private Handler<RoutingContext> uploadImg() {
        return ctx -> {
            // in your example you only handle 1 file upload, here you can handle
            // any number of uploads
            try {
                String accessToken = ctx.request().getFormAttribute("accessToken");

                checkAccessToken(accessToken);


                for (io.vertx.rxjava.ext.web.FileUpload f : ctx.fileUploads()) {
                    // do whatever you need to do with the file (it is already saved
                    // on the directory you wanted...
                    String destId = UUID.randomUUID().toString() + ".jpg";
                    String destFullPathName = WEBROOT_IMG + destId;
                    String uploadedFullPath = f.uploadedFileName();
                    log("uploadUserAvatar Filename: " + f.fileName() + uploadedFullPath + " dest full path " + destFullPathName
                            + "uploadUserAvatar Size: " + f.size());
                    File destFile = new File(destFullPathName);
                    File uploadedFile = new File(uploadedFullPath);

                    ChatUser user = db.findUserByAccessToken(accessToken);
                    if (user.imgId != null) {
                        File oldImg = new File(WEBROOT_IMG + user.imgId);
                        if (oldImg.exists()) {
                            oldImg.delete();
                        }
                    }
                    user.imgId = null;
                    db.updateImg(user);
                    //TODO use chat user
                    // Product product = db.findProduct(productId);
                    // if (product.imgId != null) {
                    //     File oldImg = new File(WEBROOT_IMG + product.imgId);
                    //     if (oldImg.exists()) {
                    //         oldImg.delete();
                    //     }
                    // }
                    // product.imgId = null;
                    // db.updateProduct(product, true);
                    //
                    if (destFile.exists()) { //TODO regenerate
                        destFile.delete();
                    }
                    uploadedFile.renameTo(destFile);
                    user.imgId = destId;
                    db.updateImg(user);
                    //   product.imgId = destId;
                    //   db.updateProduct(product, true);
                    ctx.response().end(JsonUtils.toJson(new BaseResponse(BaseResponse.OK, destId)));
                    return;
                }

                ctx.response().end(JsonUtils.toJson(new BaseResponse(BaseResponse.FAILED_TO_UPLOAD_FILE, "Файл не найден")));
            } catch (Exception e) {
                Log.e(LOG_TAG, e, "Failed to handle uploadNewProductPic");
            }

        };
    }

    private Handler<RoutingContext> refreshDisplayNameUser() {
        return c ->
                c.request().bodyHandler(v -> {
                    HttpServerResponse response = getStandardResponse(c);

                    try {

                        EditDisplayNameRequest req = JsonUtils.fromJson(v.toString(), EditDisplayNameRequest.class);
                        checkAccessToken(req.accessToken);
                        db.refreshDisplayNameUser(req.user);
                        responseEnd(c, response, new BaseResponse(BaseResponse.OK));

                    } catch (Exception e) {
                        log(e);
                        responseEnd(c, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
                    }
                });
    }

    private Handler<RoutingContext> getChatUserById() {
        return c ->
                c.request().bodyHandler(v -> {
                    HttpServerResponse response = getStandardResponse(c);

                    try {
                        GetChatUserRequest req = JsonUtils.fromJson(v.toString(), GetChatUserRequest.class);
                        checkAccessToken(req.accessToken);
                        ChatUser user = db.getUserById(req.id);
                        responseEnd(c, response, new BaseResponse(user));

                    } catch (Exception e) {
                        log(e);
                        responseEnd(c, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
                    }
                });
    }

    private Handler<RoutingContext> sync() {
        return c -> c.request().bodyHandler(v -> syncHelper.onSyncRequest(c, v));
    }

    private Handler<RoutingContext> logOut() {
        return c ->
                c.request().bodyHandler(v -> {
                    HttpServerResponse response = getStandardResponse(c);

                    try {
                        GetChatUserRequest req = JsonUtils.fromJson(v.toString(), GetChatUserRequest.class);
                        checkAccessToken(req.accessToken);
                        db.deletePushTokensById(req.id);
                        responseEnd(c, response, new BaseResponse(new SyncData()));

                    } catch (Exception e) {
                        log(e);
                        responseEnd(c, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
                    }
                });
    }

    private Handler<RoutingContext> signUp() {
        return c ->
                c.request().bodyHandler(v -> {
                    HttpServerResponse response = getStandardResponse(c);

                    try {
                        LoginRequest req = JsonUtils.fromJson(v.toString(), LoginRequest.class);
                        if (req.password.length() < 6) {
                            throw new RuntimeException("password is shorter than 6 characters");
                        }


                        if (db.findUserByLogin(req.login) != null) {
                            throw new RuntimeException("This login already used");
                        } else {
                            ChatUser user = new ChatUser(req.login, req.password);
                            db.insertChatUser(user);
                            generateAndReturnAccessToken(c, response, user);
                        }


                        return;

                    } catch (Exception e) {
                        log(e);
                        responseEnd(c, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
                    }
                });

    }

    private Handler<RoutingContext> sendMessage() {
        return c ->
                c.request().bodyHandler(v -> {
                    HttpServerResponse response = getStandardResponse(c);
                    try {
                        MessageRequest req = JsonUtils.fromJson(v.toString(), MessageRequest.class);
                        ChatUser user = checkAccessToken(req);

                        List<PushTokenInfo> pushTokens = db.getAllPushTokenForId(req.toUserId);
                        String fromLogin = user.login;
                        String fromId = user.id;

                        //FIXME TO_USER_ID CHECK USER IS EXISTED!
                        if (Utils.isEmpty(req.toUserId)) {
                            throw new RuntimeException("toUserId must be not null");
                        }

                        MessageUser msg = new MessageUser(fromId, req.toUserId, req.textMessage, System.currentTimeMillis());

                        syncHelper.onNewMessage(msg);

                        vertx.executeBlocking(e -> {
                                    for (PushTokenInfo pushToken : pushTokens) {
                                        String pushTokenStr = pushToken.pushToken;
                                        pushSenderHelper.sendPush(pushTokenStr, msg, fromLogin);
                                    }
                                    e.complete();
                                },
                                e -> {
                                    BaseResponse obj = new BaseResponse(msg);
                                    responseEnd(c, response, obj);
                                });
                    } catch (Exception e) {
                        log(e);
                        responseEnd(c, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
                    }
                });
    }

    void responseEnd(RoutingContext c, HttpServerResponse response, BaseResponse obj) {
        if (response.closed()) {
            Log.w(LOG_TAG, "trying to write in closed response");
            return;
        }
        String string = JsonUtils.toJson(obj);
        log(c.request().uri() + "> " + string);
        response.end(string);
    }

    private Handler<RoutingContext> registerPushToken() {
        return c ->
                c.request().bodyHandler(v -> {
                    HttpServerResponse response = getStandardResponse(c);
                    try {
                        RegisterPushTokenRequest req = JsonUtils.fromJson(v.toString(), RegisterPushTokenRequest.class);
                        checkAccessToken(req);
                        String userId = db.findUserByAccessToken(req.accessToken).id;
                        db.savePushToken(userId, req.pushToken);

                        responseEnd(c, response, new BaseResponse(BaseResponse.OK));

                    } catch (Exception e) {
                        log(e);
                        responseEnd(c, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
                    }
                });
    }


    private Handler<RoutingContext> getChatUsers() {
        return c ->
                c.request().bodyHandler(v -> {
                    HttpServerResponse response = getStandardResponse(c);
                    try {
                        BaseRequest req = JsonUtils.fromJson(v.toString(), BaseRequest.class);
                        checkAccessToken(req);

                        List<ChatUser> allUsers = db.getAllUsers();
                        responseEnd(c, response, new BaseResponse(allUsers));
                    } catch (Exception e) {
                        log(e);
                        responseEnd(c, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
                    }
                });
    }

    ChatUser checkAccessToken(BaseRequest req) throws SQLException {
        if (req != null) {
            return checkAccessToken(req.accessToken);
        }

        throw new RuntimeException("Access token is invalid");
    }

    private ChatUser checkAccessToken(String accessToken) throws SQLException {
        ChatUser user;
        user = db.findUserByAccessToken(accessToken);
        if (user != null) {
            return user;
        }
        throw new RuntimeException("Access token is invalid");
    }

    private Handler<RoutingContext> login() {
        return c ->
                c.request().bodyHandler(v -> {
                    HttpServerResponse response = getStandardResponse(c);

                    try {
                        LoginRequest req = JsonUtils.fromJson(v.toString(), LoginRequest.class);
                        ChatUser user = db.findUserByLoginAndPassword(req.login, req.password);
                        if (user != null) {
                            generateAndReturnAccessToken(c, response, user);

                        } else {
                            throw new RuntimeException("User not found");
                        }


                        return;

                    } catch (Exception e) {
                        log(e);
                        responseEnd(c, response, new BaseResponse(BaseResponse.UNEXPECTED_ERROR, e));
                    }
                });

    }

    private void generateAndReturnAccessToken(RoutingContext c, HttpServerResponse response, ChatUser user) throws SQLException {
        String token;
        token = UUID.randomUUID().toString().toUpperCase() + System.currentTimeMillis();
        db.addNewAccessToken(user.id, token);
        responseEnd(c, response, new BaseResponse(new LoginResponse(token, user)));
    }


    void log(Exception e) {
        Log.e(LOG_TAG, e);
    }

    HttpServerResponse getStandardResponse(RoutingContext event) {
        return event.response().putHeader("content-type", "application/json; charset=utf-8");
    }

    protected void log(String s) {
        System.out.println(s);
    }


}
