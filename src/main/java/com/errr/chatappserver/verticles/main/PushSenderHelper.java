package com.errr.chatappserver.verticles.main;

import com.errr.chatappserver.models.MessageUser;
import us.raudi.pushraven.FcmResponse;
import us.raudi.pushraven.Message;
import us.raudi.pushraven.Notification;
import us.raudi.pushraven.Pushraven;
import us.raudi.pushraven.configs.AndroidConfig;
import us.raudi.pushraven.notifications.AndroidNotification;

import java.io.File;
import java.util.HashMap;

public class PushSenderHelper {

    private final ServerVerticle serverVerticle;

    public PushSenderHelper(ServerVerticle serverVerticle) {
        this.serverVerticle = serverVerticle;            //TODO use vertx http client instead of PushRaven
    }

    public void sendPush(String pushToken, MessageUser msg, String fromLogin) {
        HashMap<String, String> data = new HashMap<>();
        data.put("fromUserId", msg.fromUserId);
        data.put("fromLogin", fromLogin);
        data.put("toUserId", msg.toUserId);
        data.put("messageId", msg.messageId);
        data.put("createdTs", "" + msg.createdTs);

        sendPush(pushToken, data, fromLogin, msg.messageBody);
    }

    private void sendPush(String pushToken, HashMap<String, String> data, String title, String body) {

        Pushraven.setAccountFile(new File("service_firebase_chatapp.json"));
        Pushraven.setProjectId("chatapp-acc1f");


        Notification not = new Notification()
                .title(title)
                .body(body);

        AndroidConfig droidCfg = new AndroidConfig()
                .notification(
                        new AndroidNotification()
                                .color("#ff0000")
                )
                .data(data)
                .priority(AndroidConfig.Priority.HIGH);

        Message raven = new Message()
                .name("4534534534")
                .notification(not)
                .token(pushToken)
                .android(droidCfg);

        FcmResponse response = Pushraven.push(raven);

        int x = 10;
    }
}
