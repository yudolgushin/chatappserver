package com.errr.chatappserver;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

public class JsonUtils {

    private static Gson gson = new GsonBuilder()
            .excludeFieldsWithModifiers(Modifier.PRIVATE, Modifier.STATIC)
            .create();
    private static Gson gsonWithNullSerialization = new GsonBuilder()
            .excludeFieldsWithModifiers(Modifier.PRIVATE, Modifier.STATIC)
            .serializeNulls()
            .create();

    public static Gson getGson(boolean withNullSerialization) {
        return withNullSerialization ? gsonWithNullSerialization : gson;
    }

    public static <T> T fromJson(String json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }

    public static <T> T fromJson(io.vertx.core.json.JsonObject json, Class<T> classOfT) {
        return gson.fromJson(json.encode(), classOfT);
    }

    public static String toJson(Object obj) {
        return gson.toJson(obj);
    }

    public static io.vertx.core.json.JsonObject toJsonObject(Object object) {
        return new io.vertx.core.json.JsonObject(gson.toJson(object));
    }

    public static <T> T fromJson(String string, Type type) {
        return gson.fromJson(string, type);
    }
}