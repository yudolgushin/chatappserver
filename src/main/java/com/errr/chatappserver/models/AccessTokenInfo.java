package com.errr.chatappserver.models;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class AccessTokenInfo {
    public static final Type LIST_TYPE = new TypeToken<List<AccessTokenInfo>>() {
    }.getType();

    public String userId;
    public String accessToken;
    public long createdTs;

    public AccessTokenInfo(String userId, String accessToken) {
        this.userId = userId;
        this.accessToken = accessToken;
        createdTs = System.currentTimeMillis();
    }
}
