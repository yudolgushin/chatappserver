package com.errr.chatappserver.models;


import java.util.UUID;

public class MessageUser {
    public String messageId;

    public String fromUserId;
    public String toUserId;
    public String messageBody;
    public long createdTs;

    public MessageUser(String fromUserId, String toUserId, String messageBody, long createdTs) {

        messageId = UUID.randomUUID().toString();
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.messageBody = messageBody;
        this.createdTs = createdTs;
    }
}
